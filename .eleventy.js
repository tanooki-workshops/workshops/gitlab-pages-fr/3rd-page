module.exports = function(eleventyConfig) {
  eleventyConfig.addPassthroughCopy('./website/css')

  return {
    passthroughFileCopy: true,
    dir: {
      input: "website",
      output: "public"
    }
  }
}